package org.rfh.service.mongodb;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.rfh.mapper.ChatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ChatServiceTest {
    @Autowired
    private ChatRepository chatRepository;

    // 测试方法
    @Test
    public  void testMethod() {
        chatRepository.findAll();
    }
}
