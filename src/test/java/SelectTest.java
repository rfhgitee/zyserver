import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.recommendation.ALS;
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel;
import org.apache.spark.mllib.recommendation.Rating;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@SpringBootTest
public class SelectTest {

    @Test
    public void selectUser(){
        SparkConf conf = new SparkConf().setAppName("rfh").setMaster("local");
        JavaSparkContext sparkContext = new JavaSparkContext(conf);
        Rating rating = new Rating(1, 2, 2.3);
        Rating rating1 = new Rating(1, 3, 2.4);
        ArrayList<Rating> list = new ArrayList<>();
        list.add(rating1);
        list.add(rating);
        JavaRDD<Rating> rdd = sparkContext.parallelize(list);

        MatrixFactorizationModel model = ALS.train(JavaRDD.toRDD(rdd), 10, 10, 0.01);

        System.out.println(model.predict(1, 3));
    }

}
