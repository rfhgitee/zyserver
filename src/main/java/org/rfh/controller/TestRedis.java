package org.rfh.controller;

import org.rfh.common.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class TestRedis {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    @GetMapping("/testredis")
    public R testredis(){
        redisTemplate.opsForValue().set("key1","value1");
        return R.success(null);
    }
}
