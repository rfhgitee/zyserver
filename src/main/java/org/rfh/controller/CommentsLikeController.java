package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.rfh.common.R;
import org.rfh.pojo.ArticleComments;
import org.rfh.pojo.CommentsLike;
import org.rfh.service.ArticleCommentsService;
import org.rfh.service.CommentsLikeService;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@RestController
@RequestMapping("/rfh/comments-like")
@CrossOrigin
public class CommentsLikeController {
    @Autowired
    private CommentsLikeService commentsLikeService;
    @Autowired
    private ArticleCommentsService articleCommentsService;
    @GetMapping("/likeArticleComment/{commentId}")
    @Transactional
    public R likeArticleComment(@PathVariable("commentId") Integer commentId) {
        // 看该用户是否已经点赞过了
        HashMap<Object, Object> map = new HashMap<>();

        Integer userId = ThreadLocalUtil.getCurrentId();
        if (userId == null){
            return R.error("请先登录",401);
        }
        LambdaQueryWrapper<CommentsLike> likeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        likeLambdaQueryWrapper.eq(CommentsLike::getCommentId,commentId)
                .eq(CommentsLike::getUserId,userId);
        CommentsLike one = commentsLikeService.getOne(likeLambdaQueryWrapper);
        if(one != null){
            //如果已经存在，那么就是取消点赞的操作。
            boolean remove = commentsLikeService.remove(likeLambdaQueryWrapper);
            if(!remove){
                return R.error("取消点赞失败",500);
            }
            R<Object> r = new R<>();
            // 更新评论的点赞数
            LambdaQueryWrapper<CommentsLike> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(CommentsLike::getCommentId,commentId);
            int count = commentsLikeService.count(queryWrapper);
            LambdaUpdateWrapper<ArticleComments> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(ArticleComments::getId,commentId)
                    .set(ArticleComments::getLikes,count);
            boolean update = articleCommentsService.update(updateWrapper);
            if(!update){
                return R.error("取消失败",500);
            }
            map.put("isLiked",false);
            map.put("count",count);
            r.setMap(map);
            r.setCode(0);
            r.setMessage("取消点赞成功");
            return r;
        }
        CommentsLike commentsLike = new CommentsLike();
        commentsLike.setCommentId(commentId);
        commentsLike.setUserId(userId);
        commentsLikeService.save(commentsLike);
        // 更新评论的点赞数
        LambdaQueryWrapper<CommentsLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(CommentsLike::getCommentId,commentId);
        int count = commentsLikeService.count(queryWrapper);
        LambdaUpdateWrapper<ArticleComments> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(ArticleComments::getId,commentId)
                .set(ArticleComments::getLikes,count);
        boolean update = articleCommentsService.update(updateWrapper);
        if(!update){
            return R.error("点赞失败",400);
        }
        R<Object> r = new R<>();
        map.put("isLiked",true);
        map.put("count",count);
        r.setMap(map);
        r.setMessage("点赞成功");
        r.setCode(0);
        return r;
    }
}

