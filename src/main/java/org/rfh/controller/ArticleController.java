package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.rfh.common.R;
import org.rfh.dto.article.*;
import org.rfh.dto.page.ArticlePageDto;
import org.rfh.mapper.ArticleLikeMapper;
import org.rfh.mapper.ArticleMapper;
import org.rfh.mapper.ArticleViewsMapper;
import org.rfh.pojo.*;
import org.rfh.service.*;
import org.rfh.utils.QiniuyunUtil;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 文章信息表，存储已发布的文章 前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-11-28
 */
@RestController
@RequestMapping("/rfh/article")
@CrossOrigin
@Slf4j
public class ArticleController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private QiniuyunUtil qiniuyunUtil;
    @Autowired
    private ArticleChannelService articleChannelService;
    @Autowired
    private ArticleContentService articleContentService;
    @Autowired
    private ApUserService apUserService;
    @Autowired
    private ArticleCommentsService articleCommentsService;
    @Autowired
    private CommentsLikeService commentsLikeService;
    @Autowired
    private ArticleViewsService articleViewsService;
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ArticleViewsMapper articleViewsMapper;
    @Autowired
    private ArticleLikeMapper articleLikeMapper;
    //分页查询
    @GetMapping("/pageList")
    public R pageList(ArticlePageDto articlePageDto){
        System.out.println(articlePageDto);
        IPage<Article> pageList = articleService.pageList(articlePageDto);
        for (Article record : pageList.getRecords()) {
            System.out.println(record);
        }
        System.out.println(pageList.getTotal());
        R<Object> r = new R<>();
        r.add("total",pageList.getTotal());
        r.add("articleList",pageList.getRecords());
        r.setCode(0);
        return r;
    }
    //添加文章
    @Transactional
    @PostMapping("/addArticle")
    public R addArticle(ArticleDto articleDto) throws IOException {
        System.out.println(articleDto);
        //首先判断文章标题是否重复
        LambdaQueryWrapper<Article> articleLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleLambdaQueryWrapper.eq(Article::getAuthorId,articleDto.getUserId())
                .eq(Article::getTitle,articleDto.getTitle());
        Article one = articleService.getOne(articleLambdaQueryWrapper);
        if(one != null) {
            return R.error("添加失败，标题已存在",400);
        }
        String imageUrl = qiniuyunUtil.setUploadManager(articleDto.getImages());
        //保存文章基本信息到文章表
        Article article = new Article();
        String title = articleDto.getTitle();
        String content = articleDto.getContent();
        Integer userId = articleDto.getUserId();
        Integer categoryId = articleDto.getCategoryId();
        String userName = articleDto.getUserName();
        //对进本信息进行赋值
        article.setAuthorId(userId);
        article.setAuthorName(userName);
        article.setTitle(title);
        article.setCategoryId(categoryId);
        article.setImages(imageUrl);
        // 根据categoryId查询分类名，并进行设置
        ArticleChannel channel = articleChannelService.getById(categoryId);
        article.setCategoryName(channel.getCategoryName());
        boolean save = articleService.save(article);
        if(!save){
            return R.error("保存失败",500);
        }
        //把内容保存在内容表中
        ArticleContent articleContent = new ArticleContent();
        articleContent.setArticleId(article.getId());
        articleContent.setContent(content);
        boolean contentSave = articleContentService.save(articleContent);
        if(!contentSave){
            return R.error("内容保存失败",500);
        }
        //保存文章内容到文章内容表
        return R.success(null);
    }
    //修改文章(图片参数为url时)
    @PostMapping("/updateArticleUrl")
    @Transactional
    public R updateArticleUrl(ArticleDto2 articleDto2) {
        System.out.println(articleDto2);
        //当图片参数为url说明图片没有被修改。
       return articleService.updateArticle(articleDto2);
    }
    //修改文章(图片参数为file时)
    @PostMapping("/updateArticleFile")
    public R updateArticleFile(ArticleDto articleDto){
        System.out.println(articleDto);
        //首先把file文件上传到七牛云，然后保存返回的url
        String imageUrl = qiniuyunUtil.setUploadManager(articleDto.getImages());
        ArticleDto2 articleDto2 = new ArticleDto2();
        BeanUtils.copyProperties(articleDto,articleDto2);
        articleDto2.setImages(imageUrl);
        return articleService.updateArticle(articleDto2);
    }
//删除文章
    @DeleteMapping("/deleteArticle/{articleId}")
    @Transactional
    public R deleteArticle(@PathVariable(value = "articleId",required = true) Long articleId) {
        log.info("文章id：{}",articleId);
        //首先根据文章id将对应的内容表中的删除
        LambdaQueryWrapper<ArticleContent> contentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        contentLambdaQueryWrapper.eq(ArticleContent::getArticleId,articleId);
        boolean remove = articleContentService.remove(contentLambdaQueryWrapper);
        if(!remove){
            return R.error("删除文章内容失败",500);
        }
        //再删除文章信息表
        boolean removeArticle = articleService.removeById(articleId);
        if(!removeArticle){
            return R.error("删除文章基本信息失败",500);
        }
        return R.success(null);
    }
    //查询文章信息
    @PostMapping("/getArticle")
    public R getAllArticle(@RequestBody ArticlePageDto articlePageDto) {
        System.out.println("==============="+articlePageDto);
        //查询所有文章
        IPage<Article> articleIPage = articleService.pageListAllArticle(articlePageDto);
        List<Article> articleList = articleIPage.getRecords();
        long total = articleIPage.getTotal();
        //创建新的文章列表信息
        ArrayList<ArticleShowDto> articleShowDtos = new ArrayList<>();
        for (Article article : articleList) {
            ArticleShowDto articleShowDto = new ArticleShowDto();
            BeanUtils.copyProperties(article, articleShowDto);
            //查询文章内容
            LambdaQueryWrapper<ArticleContent> articleContentLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleContentLambdaQueryWrapper.eq(ArticleContent::getArticleId,article.getId());
            ArticleContent articleContent = articleContentService.getOne(articleContentLambdaQueryWrapper);
            if (articleContent != null) {
                articleShowDto.setContent(articleContent.getContent());
            }
            articleShowDtos.add(articleShowDto);
        }
        System.out.println(articleShowDtos);
        R<Object> r = new R<>();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("pageList",articleShowDtos);
        r.setCode(0);
        r.setMap(map);
        return r;
    }
    //查询文章详情
    @GetMapping("/getArticleDetail/{id}")
    public R getArticleDetail(@PathVariable("id") Long id) {
        //查询文章详情
        Article article = articleService.getOne(new LambdaQueryWrapper<>(Article.class).eq(Article::getId,id));
        System.out.println(article);
        if (article == null) {
            return R.error("文章不存在",400);
        }
        ArticleDetailDto articleDetailDto = new ArticleDetailDto();
        BeanUtils.copyProperties(article, articleDetailDto);
        //根据用户id查询出作者头像
        ApUser user = apUserService.getOne(new LambdaQueryWrapper<>(ApUser.class).eq(ApUser::getId,article.getAuthorId()));
        if (user == null) {
            return R.error("用户不存在",400);
        }
        articleDetailDto.setImage(user.getImage());
        //根据文章id查询内容
        LambdaQueryWrapper<ArticleContent> articleContentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleContentLambdaQueryWrapper.eq(ArticleContent::getArticleId, id);
        ArticleContent content = articleContentService.getOne(articleContentLambdaQueryWrapper);
        if (content == null) {
            return R.error("内容不存在",400);
        }
        articleDetailDto.setContent(content.getContent());
        //将localdatetime转为String
        // 定义自定义的日期时间格式
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String createdTime = article.getCreatedTime().format(formatter);
        log.info(createdTime);
        articleDetailDto.setCreatedTime(createdTime);
        return R.success(articleDetailDto);
    }
    //查询文章评论信息
    @GetMapping("/getArticleComment/{id}")
    public R getArticleComment(@PathVariable("id") Long id) {
        //根据文章id查询评论信息
        LambdaQueryWrapper<ArticleComments> commentsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        commentsLambdaQueryWrapper.eq(ArticleComments::getArticleId,id)
                .eq(ArticleComments::getParentCommentId,0)
                .orderByDesc(ArticleComments::getCreatedTime);
        List<ArticleComments> articleComments = articleCommentsService.list(commentsLambdaQueryWrapper);
        ArrayList<ArticleCommentDto> articleCommentDtos = new ArrayList<>();
        for (ArticleComments articleComment : articleComments) {
            ArticleCommentDto articleCommentDto = new ArticleCommentDto();
            BeanUtils.copyProperties(articleComment,articleCommentDto);
            //根据用户id查询出用户名称和用户头像
            LambdaQueryWrapper<ApUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
            userLambdaQueryWrapper.eq(ApUser::getId,articleComment.getUserId());
            ApUser apUser = apUserService.getOne(userLambdaQueryWrapper);
            articleCommentDto.setImage(apUser.getImage());
            articleCommentDto.setUserName(apUser.getName());
            //将日期转为String
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String createdTime = articleComment.getCreatedTime().format(dateTimeFormatter);
            articleCommentDto.setCreatedTime(createdTime);
            //查询回复评论
            LambdaQueryWrapper<ArticleComments> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(ArticleComments::getParentCommentId,articleComment.getId())
                    .orderByDesc(ArticleComments::getCreatedTime);
            List<ArticleComments> list = articleCommentsService.list(queryWrapper);
            if(list!=null && list.size()>0){
                ArrayList<ArticleCommentDto> replayArticleCommentDtos = new ArrayList<>();
                for (ArticleComments comments : list) {
                    ArticleCommentDto replayArticleCommentDto = new ArticleCommentDto();
                    BeanUtils.copyProperties(comments,replayArticleCommentDto);
                    //根据用户id查询出用户名称和用户头像
                    System.out.println(comments.getUserId());
                    ApUser user = articleService.getUserById(comments.getUserId());
//                    LambdaQueryWrapper<ApUser> userQueryWrapper = new LambdaQueryWrapper<>();
//                    userLambdaQueryWrapper.eq(ApUser::getId,comments.getUserId());
//                    ApUser replayApUser = apUserService.getById(1);
                    replayArticleCommentDto.setImage(user.getImage());
                    replayArticleCommentDto.setUserName(user.getName());
                    //将日期转为String
                    DateTimeFormatter replayDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    String replayCreatedTime = comments.getCreatedTime().format(replayDateTimeFormatter);
                    replayArticleCommentDto.setCreatedTime(replayCreatedTime);
                    //查看该评论是否被用户点赞过
                    Integer userId   = ThreadLocalUtil.getCurrentId();
                    if (userId != null) {
                        LambdaQueryWrapper<CommentsLike> commentsLikeLambdaQueryWrapper = new LambdaQueryWrapper<>();
                        commentsLikeLambdaQueryWrapper.eq(CommentsLike::getCommentId,comments.getId())
                                .eq(CommentsLike::getUserId,userId );
                        CommentsLike one = commentsLikeService.getOne(commentsLikeLambdaQueryWrapper);
                        if (one !=null){
                            //说明该用户对该评论进行点赞了，那么我们就把isLiked设置为true
                            replayArticleCommentDto.setLiked(true);
                        }
                    }else {
                        replayArticleCommentDto.setLiked(false);
                    }
                    replayArticleCommentDtos.add(replayArticleCommentDto);
                }
                articleCommentDto.setChildrenSize(replayArticleCommentDtos.size());
                articleCommentDto.setChildren(replayArticleCommentDtos);

            }
            //查看该评论是否被该用户点赞过
            //首先需要该评论的id和用户id
            //判断用户是否登录，如果未登录那么就不用进行该判断
            Integer userId   = ThreadLocalUtil.getCurrentId();
            if (userId != null) {
                LambdaQueryWrapper<CommentsLike> commentsLikeLambdaQueryWrapper = new LambdaQueryWrapper<>();
                commentsLikeLambdaQueryWrapper.eq(CommentsLike::getCommentId,articleComment.getId())
                        .eq(CommentsLike::getUserId,userId );
                CommentsLike one = commentsLikeService.getOne(commentsLikeLambdaQueryWrapper);
                if (one !=null){
                    //说明该用户对该评论进行点赞了，那么我们就把isLiked设置为true
                    articleCommentDto.setLiked(true);
                }
            }else {
                articleCommentDto.setLiked(false);
            }
            articleCommentDtos.add(articleCommentDto);
        }
        System.out.println("articleCommentDtos"+articleCommentDtos);
        return R.success(articleCommentDtos);
    }
    //查询某用户的浏览历史文章
    @PostMapping("/history")
    public R getHistoryArticle(@RequestBody ArticlePageDto articlePageDto){
//        //通过用户id根据时间更新降序获取到文章查看列表
//        LambdaQueryWrapper<ArticleViews> articleViewsLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        articleViewsLambdaQueryWrapper.eq(ArticleViews::getUserId,articlePageDto.getUserId())
//                .orderByDesc(ArticleViews::getUpdatedTime);
//        List<ArticleViews> list = articleViewsService.list(articleViewsLambdaQueryWrapper);
//        System.out.println(list);
//        //创建一个数组专门存放文章id
//        ArrayList<Integer> ids = new ArrayList<>();
//        for (ArticleViews articleViews : list) {
//            ids.add(articleViews.getArticleId());
//        }
//        //通过id数组查找文章列表
//        //分页查询
//        Page<Article> articlePage = new Page<>();
//        //设置当前页
//        articlePage.setCurrent(articlePageDto.getPageNum());
//        //设置每一页展示多少条数据
//        articlePage.setSize(articlePageDto.getPageSize());
//
//        LambdaQueryWrapper<Article> articleLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        articleLambdaQueryWrapper.in(Article::getId,ids);
//
//        Page<Article> page = articleService.page(articlePage, articleLambdaQueryWrapper);
//        List<Article> records = page.getRecords();
//        long total = page.getTotal();
        //自己写一个分页查询
        List<Article> articles = articleMapper.selectArticleListPage(articlePageDto.getUserId(),
                (articlePageDto.getPageNum() - 1) * articlePageDto.getPageSize(),
                articlePageDto.getPageSize());
        System.out.println(articles);
        //查总条数
        Integer total = articleViewsMapper.getTotal(articlePageDto.getUserId());
        //创建新的文章列表信息
        ArrayList<ArticleShowDto> articleShowDtos = new ArrayList<>();
        for (Article article : articles) {
            ArticleShowDto articleShowDto = new ArticleShowDto();
            BeanUtils.copyProperties(article, articleShowDto);
            //查询文章内容
            LambdaQueryWrapper<ArticleContent> articleContentLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleContentLambdaQueryWrapper.eq(ArticleContent::getArticleId,article.getId());
            ArticleContent articleContent = articleContentService.getOne(articleContentLambdaQueryWrapper);
            if (articleContent != null) {
                articleShowDto.setContent(articleContent.getContent());
            }
            articleShowDtos.add(articleShowDto);
        }
        System.out.println(articleShowDtos);
        R<Object> r = new R<>();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("pageList",articleShowDtos);
        r.setCode(0);
        r.setMap(map);
        return r;
    }
    //查询该用户点赞过的文章
    @PostMapping("/like")
    public R getLikeArticle(@RequestBody ArticlePageDto articlePageDto){
        //查询用户点赞过的文章，根据created_time排序
        List<Article> articleList = articleMapper.selectLikeArticleListPage(articlePageDto.getUserId(),
                (articlePageDto.getPageNum() - 1) * articlePageDto.getPageSize(),
                articlePageDto.getPageSize());
        //文章查出来后，再查总条数
        Integer total = articleLikeMapper.getTotal(articlePageDto.getUserId());
//创建新的文章列表信息
        ArrayList<ArticleShowDto> articleShowDtos = new ArrayList<>();
        for (Article article : articleList) {
            ArticleShowDto articleShowDto = new ArticleShowDto();
            BeanUtils.copyProperties(article, articleShowDto);
            //查询文章内容
            LambdaQueryWrapper<ArticleContent> articleContentLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleContentLambdaQueryWrapper.eq(ArticleContent::getArticleId,article.getId());
            ArticleContent articleContent = articleContentService.getOne(articleContentLambdaQueryWrapper);
            if (articleContent != null) {
                articleShowDto.setContent(articleContent.getContent());
            }
            articleShowDtos.add(articleShowDto);
        }
        System.out.println(articleShowDtos);
        R<Object> r = new R<>();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("pageList",articleShowDtos);
        r.setCode(0);
        r.setMap(map);
        return r;
    }
}

