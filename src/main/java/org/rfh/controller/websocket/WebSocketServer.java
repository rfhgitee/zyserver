package org.rfh.controller.websocket;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.UpdateResult;
import com.mongodb.connection.ConnectionPoolSettings;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.rfh.pojo.Chat;
import org.rfh.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@ServerEndpoint("/chat/{id}")
@Component
public class WebSocketServer {

    //记录连接的客户端
    public static final Map<Integer, Session> clients = new ConcurrentHashMap<>();
    // 这里我是采用mongodb去存储聊天数据
    private MongoCollection<Document> collection;
    public WebSocketServer() {
        initializeMongoCollection();
    }

    private void initializeMongoCollection() {
        // 配置 MongoDB 连接池设置
        ConnectionPoolSettings connectionPoolSettings = ConnectionPoolSettings.builder()
                .maxSize(100)           // 设置连接池最大连接数
                .maxWaitTime(50, TimeUnit.MINUTES)
                .build();

        String username = "root";
        String password = "rfh";
        String host = "192.168.229.139";
        int port = 27017;
        String databaseName = "chat";
        // 使用用户名和密码创建 MongoDB 连接字符串
        String connectionString = String.format("mongodb://%s:%s@%s:%d", username, password, host, port);
        ConnectionString connectionUrl = new ConnectionString(connectionString);
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionUrl)
                .applyToConnectionPoolSettings(builder -> builder.applySettings(connectionPoolSettings))
                .build();
        MongoClient mongoClient = MongoClients.create(clientSettings);
        MongoDatabase database = mongoClient.getDatabase("chat");    // 这里设置mongodb数据库名
        collection = database.getCollection("chat");    // 设置集合名称
    }
    private ArrayList<String> msgList;
    //当有用户建立连接到服务器的时候触发
    @OnOpen
    public void onOpen(Session session,@PathParam("id") Integer id) throws IOException {
        clients.put(id, session);
        System.out.println("连接成功");
        //当用户连接成功，我们就把关于该登录用户的消息全部返回给前端
        Bson query  =Filters.or(Filters.eq("from", id), Filters.eq("to", id));
        // 执行查询
        FindIterable<Document> result = collection.find(query);
        msgList = new ArrayList<>();
        // 遍历结果
        for (Document document : result) {
            System.out.println(document.toJson());
            msgList.add(document.toJson());
        }
        session.getBasicRemote()
                .sendText(msgList.toString());
    }
    // 关闭时触发
    @OnClose
    public void onClose(Session session,@PathParam("id") Integer id) {
        clients.remove(id);
        System.out.println("连接断开");
    }
    // 当用户发送消息时触发
    @OnMessage
    public void onMessage(String params,Session session,@PathParam("id") Integer id) throws IOException {
        System.out.println("收到消息：" + params);
        // 将收到前端的消息解析取出
        JSONObject jsonObject = JSONUtil.parseObj(params);
        Integer from = jsonObject.getInt("from");
        Integer to = jsonObject.getInt("to");
        String time = jsonObject.getStr("time");
        String fromAvatar = jsonObject.getStr("fromAvatar");
        String toAvatar = jsonObject.getStr("toAvatar");
        String msg = jsonObject.getStr("msg");
        //随机生成chatId
        String chatId = UUID.randomUUID().toString();
        // 存储消息
        // 新建对象将要存储到数据库的内容封装到一起

        jsonObject.set("chatId", chatId);
//        insertChatData(jsonObject, chatId);
        collection.insertOne(new Document()
                .append("chatId", chatId)
                .append("from", from)
                .append("to", to)
                .append("time", time)
                .append("fromAvatar", fromAvatar)
                .append("toAvatar", toAvatar)
                .append("msg", msg));
        // 判断该用户是否在线，在线则向客户端发送消息
        Session toSession = clients.get(to);
        if (toSession != null){
            toSession.getBasicRemote().sendText(jsonObject.toString());
        }
    }
    // 这是mongodb存到数据库的操作
    private void insertChatData(JSONObject chatData, String chatId) {
        Document document = Document.parse(chatData.toString());

        Document query = new Document("chatId", chatId);
        Document update = new Document("$push", new Document("chatData", document));

        UpdateResult updateResult = collection.updateOne(query, update, new UpdateOptions().upsert(true));
        System.out.println(updateResult);
    }
    public static LocalDateTime parseLocalDateTime(String dateStr) {
        DateTime parse = DateUtil.parse(dateStr);
        Instant instant = parse.toInstant();
        ZoneId zoneId = ZoneId.systemDefault();
        LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
        System.out.println(localDateTime);
        return localDateTime;
    }
    @OnError
    public void onError(Session session, Throwable error){
        error.printStackTrace();
    }

}
