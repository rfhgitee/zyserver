package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.rfh.common.R;
import org.rfh.dto.article.ArticleChannelDto;
import org.rfh.pojo.Article;
import org.rfh.pojo.ArticleChannel;
import org.rfh.service.ArticleChannelService;
import org.rfh.service.ArticleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-11-23
 */
@RestController
@RequestMapping("/rfh/article-channel")
@CrossOrigin
public class ArticleChannelController {
    @Autowired
    private ArticleService articleService;
    @Autowired
    private ArticleChannelService articleChannelService;
    //获取用户分类
    @GetMapping("/list/{id}")
    public R getArticleListById(@PathVariable("id") Integer id){
        //根据用户id查询文章分类
        System.out.println(id);
        LambdaQueryWrapper<ArticleChannel> channelLambdaQueryWrapper = new LambdaQueryWrapper<>();
        List<ArticleChannel> articleChannelList = articleChannelService.list(channelLambdaQueryWrapper.eq(ArticleChannel::getUserId, id));
        //使用工具类赋值
        List<ArticleChannelDto> articleChannelDtoList = new ArrayList<>();
        for (ArticleChannel articleChannel : articleChannelList) {
            ArticleChannelDto articleChannelDto = new ArticleChannelDto();
            BeanUtils.copyProperties(articleChannel,articleChannelDto);
            articleChannelDtoList.add(articleChannelDto);
        }
        System.out.println(articleChannelDtoList);
        return R.success(articleChannelDtoList);
    }
    //获取所有分类
    @GetMapping("/list")
    public R getArticleList(){
        List<ArticleChannel> articleChannels = articleChannelService.list();

        return R.success(articleChannels);
    }
    //添加分类
    @PostMapping("/addChannel")
    public R addChannel(@RequestBody ArticleChannelDto articleChannelDto){
        System.out.println(articleChannelDto);
        //根据分类名查询数据
        LambdaQueryWrapper<ArticleChannel> articleChannelLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ArticleChannel one = articleChannelService.
                getOne(articleChannelLambdaQueryWrapper.eq(ArticleChannel::getCategoryName, articleChannelDto.getCategoryName())
                        .eq(ArticleChannel::getUserId,articleChannelDto.getUserId()));
        if(one != null){
            return R.error("分类已存在",402);
        }
        //添加
        ArticleChannel articleChannel = new ArticleChannel();
        BeanUtils.copyProperties(articleChannelDto,articleChannel);
        boolean save = articleChannelService.save(articleChannel);
        if(!save){
            return R.error("添加失败",500);
        }
        return R.success(null);
    }
    //编辑分类
    @PostMapping("/updateChannel")
    @Transactional
    public R updateChannel(@RequestBody ArticleChannelDto articleChannelDto) {
        System.out.println(articleChannelDto);
        ArticleChannel articleChannel = new ArticleChannel();
        //首先根据categoryId拿出channel中categoryName
        ArticleChannel oldChannel = articleChannelService.getById(articleChannelDto.getCategoryId());
        //然后判断传过来的categoryName是否和原本取出来的categoryName一致，如果一致，则可以直接进行修改，如果不一致则需要进行后续的是否重名的判断。
        if(!oldChannel.getCategoryName().equals(articleChannelDto.getCategoryName())){
            //取出分类名，与数据库中的进行判断，看是否存在.
            String categoryName = articleChannelDto.getCategoryName();
            LambdaQueryWrapper<ArticleChannel> wrapper = new LambdaQueryWrapper<>();
            ArticleChannel channel = articleChannelService.getOne(wrapper.eq(ArticleChannel::getCategoryName, categoryName)
                    .eq(ArticleChannel::getUserId, articleChannelDto.getUserId()));
            //如果有，则编辑失败，提示该分类已存在。
            if(channel != null) {
                return  R.error("该分类已存在，编辑失败",402);
            }
            //根据categoryId对channel进行修改
            BeanUtils.copyProperties(articleChannelDto,articleChannel);
            boolean update = articleChannelService.updateById(articleChannel);
            if(!update){
                return R.error("修改失败",500);
            }
            //先查询文章分类是否有文章
            //没有对应的文章直接返回修改成功
            LambdaQueryWrapper<Article> articleLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleLambdaQueryWrapper.eq(Article::getCategoryId,articleChannelDto.getCategoryId());
            List<Article> articleList = articleService.list(articleLambdaQueryWrapper);

            if(articleList.size() == 0) {
                return R.success(null);
            }
            //有对应的文章，修改文章表中的文章分类
            LambdaUpdateWrapper<Article> articleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
                //根据当前用户和分类id进行修改
            articleLambdaUpdateWrapper.eq(Article::getCategoryId,articleChannelDto.getCategoryId())
                    .eq(Article::getAuthorId,articleChannelDto.getUserId())
                    .set(Article::getCategoryName,articleChannelDto.getCategoryName());
            boolean b = articleService.update(articleLambdaUpdateWrapper);
            if(!b){
                return R.error("对应文章相关数据修改失败",500);
            }

            return R.success(null);
        }
        //直接进行修改,不用判断是否重名的问题
        BeanUtils.copyProperties(articleChannelDto,articleChannel);
        boolean update = articleChannelService.updateById(articleChannel);
        if(!update){
            return R.error("修改失败",500);
        }
        return R.success(null);

    }
    //删除分类
    @DeleteMapping("/deleteChannel/{categoryId}")
    public R deleteChannel(@PathVariable("categoryId") Integer categoryId){
        System.out.println(categoryId);
        //初步实现删除，后续将会和文章进行关联看是否能删除
        boolean remove = articleChannelService.removeById(categoryId);
        if(!remove){
            return R.error("服务异常，删除失败",500);
        }
        return R.success(null);
    }
}

