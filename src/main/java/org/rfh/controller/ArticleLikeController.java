package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.rfh.common.R;
import org.rfh.pojo.Article;
import org.rfh.pojo.ArticleLike;
import org.rfh.service.ArticleLikeService;
import org.rfh.service.ArticleService;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@RestController
@RequestMapping("/rfh/article-like")
@CrossOrigin
public class ArticleLikeController {
    @Autowired
    private ArticleLikeService articleLikeService;
    @Autowired
    private ArticleService articleService;
    //点赞文章
    @GetMapping("/likeArticle/{articleId}")
    @Transactional
    public R likeArticle(@PathVariable("articleId") Long articleId) {
        System.out.println(articleId);
        HashMap<String, Boolean> hashMap = new HashMap<>();

        //首先查看文章点赞表，看是否该登录用户是否已经对该文章点赞过了
        //从ThreadLocal中取出用户id
        Integer userId  = ThreadLocalUtil.getCurrentId();
        System.out.println(userId);
        LambdaQueryWrapper<ArticleLike> likeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        likeLambdaQueryWrapper.eq(ArticleLike::getArticleId,articleId)
                .eq(ArticleLike::getUserId,userId);
        ArticleLike articleLike = articleLikeService.getOne(likeLambdaQueryWrapper);
        if(articleLike != null){
            hashMap.put("isLike",true);
            R<Object> r = new R<>();
            r.setCode(0);
            r.setMap(hashMap);
            r.setMessage("已经点赞过了");
            return r;
        }
        //添加记录到文章点赞表中，并修改文章的点赞数
        ArticleLike articleLikeNew = new ArticleLike();
        articleLikeNew.setArticleId(articleId.intValue());
        articleLikeNew.setUserId(userId);
        articleLikeService.save(articleLikeNew);
        //查询文章的点赞数
        LambdaQueryWrapper<ArticleLike> articleLikeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleLikeLambdaQueryWrapper.eq(ArticleLike::getArticleId,articleId);
        int count = articleLikeService.count(articleLikeLambdaQueryWrapper);
        //修改该文章的点赞数
        LambdaUpdateWrapper<Article> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(Article::getId,articleId)
                .set(Article::getLikes,count);
        boolean update = articleService.update(updateWrapper);
        if(!update){
            return R.error("点赞失败",500);
        }
        return R.success(count);
    }
    //取消对文章的点赞
    @DeleteMapping("/cancelLikeArticle/{articleId}")
    @Transactional
    public R  cancelLikeArticle(@PathVariable("articleId") Long articleId){
        //拿到userId
        Integer id = ThreadLocalUtil.getCurrentId();
        //判断用户是否已经点过赞
        LambdaQueryWrapper<ArticleLike> articleLikeLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleLikeLambdaQueryWrapper.eq(ArticleLike::getArticleId,articleId)
                .eq(ArticleLike::getUserId,id);
        ArticleLike articleLike = articleLikeService.getOne(articleLikeLambdaQueryWrapper);
        if(articleLike==null){
            return R.error("用户未点赞",500);
        }
        //删除点赞记录，并修改该文章的点赞数
        boolean remove = articleLikeService.remove(articleLikeLambdaQueryWrapper);
        if(!remove){
            return R.error("取消点赞失败",500);
        }
        //修改文章的点赞数
        LambdaQueryWrapper<ArticleLike> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ArticleLike::getArticleId,articleId);
        int count = articleLikeService.count(queryWrapper);
        LambdaUpdateWrapper<Article> articleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        articleLambdaUpdateWrapper.eq(Article::getId,articleId)
                .set(Article::getLikes,count);
        boolean update = articleService.update(articleLambdaUpdateWrapper);
        if(!update){
            return R.error("取消点赞失败",500);
        }
        return R.success(count);
    }
}

