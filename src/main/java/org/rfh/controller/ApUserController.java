package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import io.jsonwebtoken.Claims;
import org.rfh.common.R;
import org.rfh.dto.LoginDto;
import org.rfh.dto.user.PasswordDto;
import org.rfh.dto.user.UserInfo;
import org.rfh.dto.user.UserInfoVo;
import org.rfh.mapper.ApUserMapper;
import org.rfh.pojo.ApUser;
import org.rfh.pojo.ApUserFan;
import org.rfh.pojo.Article;
import org.rfh.service.ApUserFanService;
import org.rfh.service.ApUserService;

import org.rfh.service.ArticleService;
import org.rfh.utils.JwtUtil;
import org.rfh.utils.MD5Utils;
import org.rfh.utils.QiniuyunUtil;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * APP用户信息表 前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-11-21
 */
@RestController
@RequestMapping("/rfh/ap-user")
@CrossOrigin
public class ApUserController {
    @Autowired
    private ApUserService apUserService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private QiniuyunUtil qiniuyunUtil;
    @Autowired
    private RedisTemplate<String,String> redisTemplate;
    private Integer count = 0;
    @Autowired
    private ApUserFanService apUserFanService;
    //登录
    @PostMapping("/login")
    @Transactional
    public R login(@RequestBody LoginDto loginDto){
        System.out.println(loginDto);
        //1. 根据用户名查询用户
        LambdaQueryWrapper<ApUser> apUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ApUser apUser = apUserService.getOne(apUserLambdaQueryWrapper.eq(ApUser::getName, loginDto.getUsername()));
        //2.1 若改用户不存在，就直接返回error，并返回相关错误消息。
        if (apUser == null) {
            return  R.error("用户不存在",401);
        }
        //2.2 若用户存在，则取出该用户的salt。
        String salt = apUser.getSalt();
        //2.2.1 用传过来的password进行md5加密并进行加盐，进行判断
        String encode = MD5Utils.encode(loginDto.getPassword());
        String password = MD5Utils.encodeWithSalt(encode,salt);
        //3.1 若密码错误，直接返回error，并返回错误相关信息
        if (!password.equals(apUser.getPassword())) {
            return  R.error("密码错误",401);
        }
        //3.2 若密码正确，则根据id生成token并且返回success。
        String id = apUser.getId() + "";
        String token = JwtUtil.generateToken(id);
        String key = id + "token";
        //3.3把token保存在redis中并设置过期时间为三十分钟 id为key，token为value
        redisTemplate.opsForValue().set(key,token,30, TimeUnit.MINUTES);
        System.out.println(token);
        //3.4 还要将count-1,然后重新进行设置

        //4 返回用户信息和token给前端进行保存
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(apUser,userInfo);
        System.out.println(userInfo);
        R r = new R();
        r.add("token",token);
        r.add("userInfo",userInfo);
        r.setCode(0);
        //登陆成功将count+1
        count += 1;
        System.out.println(count);
        return r;
    }
    @PostMapping("/register")
    @Transactional
    public R register(@RequestBody LoginDto loginDto){
        System.out.println(loginDto);
        String salt = "rfh";
        //1.查询是否存在该用户
        LambdaQueryWrapper<ApUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        ApUser apUser = apUserService.getOne(userLambdaQueryWrapper.eq(ApUser::getName, loginDto.getUsername()));
        //1.1若存在，返回注册失败，用户名已占用
        if(apUser != null){
            return R.error("用户已存在",409);
        }
        //1.2若不存在，则对用户进行注册。
        ApUser newApUser = new ApUser();
        newApUser.setName(loginDto.getUsername());
        newApUser.setStatus(false);
        newApUser.setFlag(false);
        //1.2.1对密码进行md5加密然后对数据进行存储，其他信息均为默认值。
        String encode = MD5Utils.encode(loginDto.getPassword());
        //再加盐
        String password = MD5Utils.encodeWithSalt(encode, salt);
        newApUser.setSalt(salt);
        newApUser.setPassword(password);
        boolean save = apUserService.save(newApUser);
        if(!save) {
            return R.error("服务异常保存失败",500);
        }

        return R.success(null);
    }
    //退出
    @GetMapping("/logout/{id}")
    public R logout(@PathVariable("id") Integer id){
        System.out.println("===========" +id);
        //首先将count -1 然后判断count是否等于0，如果等于0那么才删除redis中的token
        count -= 1;
        System.out.println(count);
        if(count == 0){
            //删除redis中存储的token
            String userId = id + "";
            String key = userId + "token";
            Boolean delete = redisTemplate.delete(key);
            if(!delete){
                return R.error("退出失败",500);
            }
            return R.success(null);
        }
        return R.success(null);
    }
    //修改个人信息
    @PostMapping("/updateUserProfile")
    @Transactional
    public R updateUserProfile(@RequestBody UserInfo userInfo){
        //修改个人信息 首先 如果 用户名没修改，那么文章表就可以不用修改作者名
        //如果修改，则需要进行判断新的用户名是否存在重名，并修改对应的文章表

        ApUser apUser = apUserService.getOne(new LambdaQueryWrapper<ApUser>().eq(ApUser::getId,userInfo.getId()));
        System.out.println(apUser);
        //判断用户名是否修改过
        if(apUser.getName().equals(userInfo.getName())){
            //只需要修改用户表
            LambdaUpdateWrapper<ApUser> apUserLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
            apUserLambdaUpdateWrapper.eq(ApUser::getId,userInfo.getId())
                    .set(ApUser::getSex,userInfo.getSex())
                    .set(ApUser::getPhone,userInfo.getPhone());
            boolean updateProfile = apUserService.update(apUserLambdaUpdateWrapper);
            if(!updateProfile){
                return  R.error("修改个人信息失败",500);
            }
            //修改成功，重新查询用户传给前端
            ApUser newUser = apUserService.getOne(new LambdaQueryWrapper<ApUser>().eq(ApUser::getId,userInfo.getId()));
            UserInfo info = new UserInfo();
            BeanUtils.copyProperties(newUser,info);
            return R.success(info);
        }
        //判断是否重名，重名直接返回error
        LambdaQueryWrapper<ApUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(ApUser::getName,userInfo.getName());
        ApUser user = apUserService.getOne(userLambdaQueryWrapper);
        if(user != null) {
            return R.error("用户存在,修改失败",500);
        }
        //批量修改文章中的用户名
        LambdaUpdateWrapper<Article> articleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        articleLambdaUpdateWrapper.eq(Article::getAuthorId,userInfo.getId())
                .set(Article::getAuthorName,userInfo.getName());
        boolean updateArticle = articleService.update(articleLambdaUpdateWrapper);
        if(!updateArticle){
            return R.error("文章相关用户信息修改失败",500);
        }
        //修改用户信息
        LambdaUpdateWrapper<ApUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(ApUser::getId,userInfo.getId())
                .set(ApUser::getName,userInfo.getName())
                .set(ApUser::getSex,userInfo.getSex())
                .set(ApUser::getPhone,userInfo.getPhone());
        boolean update = apUserService.update(updateWrapper);
        if(!update){
            return R.error("用户信息修改失败",500);
        }
        //查出新的用户信息进行返回
        LambdaQueryWrapper<ApUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ApUser::getId,userInfo.getId());
        ApUser newUser = apUserService.getOne(queryWrapper);
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(newUser,info);
        return R.success(info);
    }
    //上传用户头像
    @PostMapping("/uploadAvatar")
    @Transactional
    public R uploadAvatar(Integer id, MultipartFile avatar){
        System.out.println(avatar);
        System.out.println(id);
        if(!(avatar instanceof MultipartFile)){
            return R.error("无效上传",400);
        }
        //保存头像到七牛云，返回url地址保存在image中
        String image = qiniuyunUtil.setUploadManager(avatar);
        LambdaUpdateWrapper<ApUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(ApUser::getId,id)
                        .set(ApUser::getImage,image);
        boolean update = apUserService.update(updateWrapper);
        if(!update){
            R.error("头像修改失败",500);
        }
        R<Object> r = new R<>();
        r.add("avatarUrl",image);
        r.setCode(0);

        return r;
    }
    //重置密码
    @PostMapping("/updatePassword")
    @Transactional
    public R updatePassword(@RequestBody PasswordDto passwordDto){
        System.out.println(passwordDto);
        //判断旧密码是否正确
        LambdaQueryWrapper<ApUser> apUserLambdaQueryWrapper = new LambdaQueryWrapper<>();
        apUserLambdaQueryWrapper.eq(ApUser::getId,passwordDto.getId());
        ApUser user = apUserService.getOne(apUserLambdaQueryWrapper);
        String salt = user.getSalt();
        String encode = MD5Utils.encode(passwordDto.getOldPassword());
        String encodeWithSalt = MD5Utils.encodeWithSalt(encode, salt);
        if(!(encodeWithSalt.equals(user.getPassword()))) {
            return R.error("原密码错误",400);
        }
        //如果正确那么将新密码替换旧密码
        String newEncode = MD5Utils.encode(passwordDto.getNewPassword());
        String password = MD5Utils.encodeWithSalt(newEncode, salt);
        LambdaUpdateWrapper<ApUser> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(ApUser::getId,passwordDto.getId())
                .set(ApUser::getPassword,password);
        boolean update = apUserService.update(updateWrapper);
        if(!update){
            return R.error("重置失败",500);
        }
        return R.success(null);
    }
    //检查是否在一处已经登录了
    @GetMapping("/checkLogin")
    public R checkLogin(){

        String token = redisTemplate.opsForValue().get("token");
        //判断token是否存在
        if(token == null){
            return R.error("游客登录",0);
        }
        //解析token,将用户信息返回给前端
        Claims claims =(Claims) JwtUtil.parseToken(token);
        String userId =(String) claims.get("userId");
        Integer id = new Integer(userId);
        //查询用户信息
        LambdaQueryWrapper<ApUser> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(ApUser::getId,id);
        ApUser apUser = apUserService.getOne(userLambdaQueryWrapper);
        if(apUser == null){
            return R.error("系统错误",500);
        }
        UserInfo userInfo = new UserInfo();
        BeanUtils.copyProperties(apUser,userInfo);
        //并且返回token
        R<Object> r = new R<>();
        HashMap<Object, Object> map = new HashMap<>();
        map.put("token",token);
        map.put("userInfo",userInfo);
        r.setMap(map);
        r.setCode(0);
        //自动登录成功 count也+1
        count += 1;
        System.out.println(count);

        return r;
    }
    //获取用户列表
    @GetMapping("/getUserList")
    public R getUserList(){
        //得到当前用户id，获取用户列表是，排除自己
        Integer id = ThreadLocalUtil.getCurrentId();
        //如果id为null,那么全查出来
        //查询用户列表
        LambdaQueryWrapper<ApUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ne(id != null ,ApUser::getId,id);
        List<ApUser> list = apUserService.list(lambdaQueryWrapper);
        //创建新的集合用于存放userInfoVo
        ArrayList<UserInfoVo> userInfoVos = new ArrayList<>();
        //user转为userinfovo
        for (ApUser apUser : list) {
            UserInfoVo userInfoVo = new UserInfoVo();
            BeanUtils.copyProperties(apUser,userInfoVo);
            //如果id为null那就不用判断是否关注，一律为false
            if(id == null){
                userInfoVo.setIsFollow(false);
            }else {
                //判断该用户是否被当前登录用户所关注
                LambdaQueryWrapper<ApUserFan> fanLambdaQueryWrapper = new LambdaQueryWrapper<>();
                //这里注意，登录用户id对应的是fanId
                fanLambdaQueryWrapper.eq(ApUserFan::getFansId,id).eq(ApUserFan::getUserId,apUser.getId());
                ApUserFan fan = apUserFanService.getOne(fanLambdaQueryWrapper);
                //不等于null说明关注了
                if(fan != null){
                    userInfoVo.setIsFollow(true);
                }else {
                    //相反，等于null就是还没关注
                    userInfoVo.setIsFollow(false);
                }
            }

            userInfoVos.add(userInfoVo);
        }

        return R.success(userInfoVos);
    }
}

