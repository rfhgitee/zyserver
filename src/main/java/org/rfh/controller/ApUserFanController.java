package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.rfh.common.R;
import org.rfh.pojo.ApUser;
import org.rfh.pojo.ApUserFan;
import org.rfh.service.ApUserFanService;
import org.rfh.service.ApUserService;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * APP用户粉丝信息表 前端控制器
 * </p>
 *
 * @author rfh
 * @since 2024-01-29
 */
@RestController
@RequestMapping("/rfh/ap-user-fan")
@CrossOrigin
public class ApUserFanController {
    @Autowired
    private ApUserFanService apUserFanService;
    @Autowired
    private ApUserService apUserService;
    //取消关注
    @DeleteMapping("/cancelFollow/{userId}")
    public R cancelFollow(@PathVariable("userId") Integer userId){
        //获取当前登录用户的id，为空则未登录
        Integer currentId = ThreadLocalUtil.getCurrentId();
        if(currentId == null){
            return R.error("请先登录",401);
        }
        //取消关注
        LambdaQueryWrapper<ApUserFan> fanLambdaQueryWrapper = new LambdaQueryWrapper<>();
        fanLambdaQueryWrapper.eq(ApUserFan::getUserId,userId)
                .eq(ApUserFan::getFansId,currentId);
        boolean remove = apUserFanService.remove(fanLambdaQueryWrapper);
        if(!remove){
            return R.error("取消关注失败",500);
        }

        return R.success("取消关注成功");
    }
    //关注
    @PostMapping("/follow/{userId}")
    public R follow(@PathVariable("userId") Integer userId){
        LambdaQueryWrapper<ApUserFan> apUserFanLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //获取当前登录用户的id，为空则未登录
        Integer currentId = ThreadLocalUtil.getCurrentId();
        if(currentId == null){
            return R.error("请先登录",401);
        }
        ApUserFan apUserFan = new ApUserFan();
        apUserFan.setUserId(userId);
        apUserFan.setFansId(currentId);
        //查询登陆人员的名称
        ApUser apUser = apUserService.getOne(new LambdaQueryWrapper<ApUser>().eq(ApUser::getId, currentId));
        apUserFan.setFansName(apUser.getName());
        apUserFan.setLevel(false);
        apUserFan.setIsDisplay(true);
        apUserFan.setIsShieldComment(false);
        apUserFan.setIsShieldLetter(false);
        boolean save = apUserFanService.save(apUserFan);
        if (!save){
            return R.error("关注失败",500);
        }
        return R.success("关注成功");
    }
}

