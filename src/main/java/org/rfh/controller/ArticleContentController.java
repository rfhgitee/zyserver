package org.rfh.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.rfh.common.R;
import org.rfh.pojo.ArticleContent;
import org.rfh.service.ArticleContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rfh/article-content")
@CrossOrigin
public class ArticleContentController {
    @Autowired
    private ArticleContentService articleContentService;
    @GetMapping("/getContent/{articleId}")
    public R getContent(@PathVariable("articleId") Integer articleId) {
        System.out.println(articleId);
        //根据文章id查询文章内容
        LambdaQueryWrapper<ArticleContent> contentLambdaQueryWrapper = new LambdaQueryWrapper<>();
        contentLambdaQueryWrapper.eq(ArticleContent::getArticleId,articleId);
        ArticleContent articleContent = articleContentService.getOne(contentLambdaQueryWrapper);
        if(articleContent == null) {
            return  R.error("文章内容查询失败",500);
        }
        return R.success(articleContent);
    }
}
