package org.rfh.controller;


import org.rfh.common.R;
import org.rfh.pojo.ArticleComments;
import org.rfh.service.ArticleCommentsService;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
@RestController
@RequestMapping("/rfh/article-comments")
@CrossOrigin
public class ArticleCommentsController {
    @Autowired
    private ArticleCommentsService articleCommentsService;
    //添加评论
    @GetMapping("/addArticleComment")
    public R addArticleComment(String commentText,Long articleId){
        if(ThreadLocalUtil.getCurrentId() == null){
            return R.error("请先登录",0);
        }
        System.out.println(commentText + "=======" + articleId);
        ArticleComments articleComments = new ArticleComments();
        articleComments.setArticleId(articleId.intValue());
        articleComments.setContent(commentText);
        articleComments.setUserId(ThreadLocalUtil.getCurrentId());
        articleComments.setParentCommentId(0);
        boolean save = articleCommentsService.save(articleComments);
        if (!save){
            return R.error("评论失败",500);
        }
        return R.success(null);
    }
    //添加回复评论
    @GetMapping("/addReplyArticleComment")
    public R addReplyArticleComment(String replyText,Long articleId,Long parentCommentId){
        System.out.println(replyText+"=======" + articleId + "=======" + parentCommentId);
        //判断是否登录
        Integer userId = ThreadLocalUtil.getCurrentId();
        if (userId ==null) {
            return R.error("请先登录",401);
        }
        ArticleComments articleComments = new ArticleComments();
        articleComments.setContent(replyText);
        articleComments.setParentCommentId(parentCommentId.intValue());
        articleComments.setArticleId(articleId.intValue());
        articleComments.setUserId(userId);
        boolean save = articleCommentsService.save(articleComments);
        if (!save){
            return R.error("回复失败",500);
        }
        return R.success(null);
    }
}

