package org.rfh.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
@RestController
@RequestMapping("/rfh/comments")
public class CommentsController {

}

