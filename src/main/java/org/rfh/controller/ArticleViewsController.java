package org.rfh.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import org.rfh.common.R;
import org.rfh.pojo.Article;
import org.rfh.pojo.ArticleViews;
import org.rfh.service.ArticleService;
import org.rfh.service.ArticleViewsService;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author rfh
 * @since 2023-12-20
 */
@RestController
@RequestMapping("/rfh/article-views")
@CrossOrigin
public class ArticleViewsController {
    @Autowired
    private ArticleViewsService articleViewsService;
    @Autowired
    private ArticleService articleService;
    private final static Integer VISITOR_ID = 0;
    //浏览量加一
    @GetMapping("/updateViews/{articleId}")
    @Transactional
    public R updateViews(@PathVariable("articleId") Long articleId){
        //判断是否已经登录
        Integer userId = ThreadLocalUtil.getCurrentId();
        //如果没有登录，那么说明是游客登陆，我们统一把游客登陆的浏览量放在userId为0的数据中
        if(userId == null){
            //首先判断是否有该数据，如果有则修改count,没有则添加
            LambdaQueryWrapper<ArticleViews> articleViewsLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleViewsLambdaQueryWrapper.eq(ArticleViews::getUserId,VISITOR_ID)
                    .eq(ArticleViews::getArticleId,articleId.intValue());
            ArticleViews one = articleViewsService.getOne(articleViewsLambdaQueryWrapper);
            if(one != null) {
                one.setViewCount(one.getViewCount() + 1);
                boolean b = articleViewsService.updateById(one);
                if(!b){
                    return R.error("更新失败",500);
                }
            }else {
                ArticleViews articleViews = new ArticleViews();
                articleViews.setUserId(VISITOR_ID);
                articleViews.setArticleId(articleId.intValue());
                articleViews.setViewCount(1);
                articleViews.setUpdatedTime(LocalDateTime.now());
                boolean b = articleViewsService.save(articleViews);
                if(!b){
                    return R.error("更新失败",500);
                }
            }
        }
        //如果登录了,看改用户浏览过当前文章吗，
        //如果浏览过，那么把viewCount+1
        //如果没有浏览过，那么添加一条记录
        else {
            LambdaQueryWrapper<ArticleViews> articleViewsLambdaQueryWrapper = new LambdaQueryWrapper<>();
            articleViewsLambdaQueryWrapper.eq(ArticleViews::getUserId,userId)
                    .eq(ArticleViews::getArticleId,articleId.intValue());
            ArticleViews one = articleViewsService.getOne(articleViewsLambdaQueryWrapper);
            if(one != null) {
                one.setViewCount(one.getViewCount() + 1);
                boolean b = articleViewsService.updateById(one);
                if(!b){
                    return R.error("更新失败",500);
                }
            }else {
                ArticleViews articleViews = new ArticleViews();
                articleViews.setArticleId(articleId.intValue());
                articleViews.setUserId(userId);
                articleViews.setViewCount(1);
                articleViews.setUpdatedTime(LocalDateTime.now());
                boolean b = articleViewsService.save(articleViews);
                if(!b) {
                    return R.error("更新失败", 500);
                }
            }
        }
        //浏览表改了,现在需要修改对应文章的浏览量
        //首先从浏览表中取出浏览过该文章的数据，然后把count全部加起来
        LambdaQueryWrapper<ArticleViews> articleViewsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        articleViewsLambdaQueryWrapper.eq(ArticleViews::getArticleId,articleId.intValue());
        List<ArticleViews> articleViewsList = articleViewsService.list(articleViewsLambdaQueryWrapper);
        int views = 0;
        for (ArticleViews articleViews : articleViewsList) {
            views +=  articleViews.getViewCount();
        }
        LambdaUpdateWrapper<Article> articleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        articleLambdaUpdateWrapper.eq(Article::getId,articleId.intValue())
                .set(Article::getViews,views);
        boolean b = articleService.update(articleLambdaUpdateWrapper);
        if(!b) {
            return  R.error("更新失败", 500);
        }
        return R.success(views);
    }
}

