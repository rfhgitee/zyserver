package org.rfh.interceptor;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.lettuce.core.RedisCommandTimeoutException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.rfh.common.R;
import org.rfh.utils.JwtUtil;
import org.rfh.utils.ThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class MyInterceptor implements HandlerInterceptor
{
    private RedisTemplate<String, String> redisTemplate;
    public MyInterceptor(RedisTemplate<String, String> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //解决跨域问题
        String origin = request.getHeader("Origin");
        log.info("origin: {}", origin);
        response.setHeader("Access-Control-Allow-Origin", origin);
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With,accept,Origin,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,Access-Token,token");
        response.setHeader("Access-Control-Expose-Headers", "*");//响应客户端的头部 允许携带Token 等等
        response.setHeader("Access-Control-Allow-Credentials", "true");  // 允许携带cookie
        response.setHeader("Access-Control-Max-Age", "3600");   // 预检请求的结果缓存时间
        String requestURI = request.getRequestURI();


        //1.拿到请求地址。
        //1.1判断否包含/login
        //1.1.1 包含/login，就放行
        if(requestURI.contains("/login") || requestURI.contains("register") || requestURI.contains("/checkLogin")) {
            return true;
        }
        //1.1.2 不包含，我们就要对token进行验证。
        //2. 拿到请求头中的token信息
        String token = request.getHeader("token");
        System.out.println(token);
        //2.1 判断是否有token
        //并且还要判断是否包含5173，如果不包含，那么也放行
        if(StringUtils.isBlank(token) && origin.contains("5173")) {
            return false;
        }
        String id = "";
        //判断token是否为空，
        //这里token有两种情况，一种是5173访问，一种是5174访问，所以我们都要进行判断
        //如果不为空切没有过期，那么就将id保存在ThreadLocal中
        if(!StringUtils.isBlank(token)){
            //2.2 如果有，判断token是否有效。
            //从token中取出id
            Claims parseToken =(Claims) JwtUtil.parseToken(token);
             id =(String) parseToken.get("userId");
            System.out.println(id);
            System.out.println(redisTemplate);
            //从redis中取出id对应的token
            String key = id + "token";
            String oldToken = redisTemplate.opsForValue().get(key);
            if( oldToken == null || !oldToken.equals(token)  ){
                R<Object> r = new R<>();
                r.setCode(401);
                r.setMessage("登录过期，请重新登录");
                r.setData(null);
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(JSON.toJSONString(r));
                return false;
            }
            //保存用户id到ThreadLocal中
            if(!StringUtils.isBlank(id)){
                Integer userId = Integer.valueOf(id);
                ThreadLocalUtil.setCurrentId(userId);
            }
            String akey = id + "token";
            redisTemplate.opsForValue().set(akey,token,30, TimeUnit.MINUTES);
            return true;
        }
        //没有token
        //5174游客登陆
        return true;
}

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
