package org.rfh.config;

import org.rfh.interceptor.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Autowired
    private  RedisTemplate<String,String> redisTemplate;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加自己的拦截器
        MyInterceptor myInterceptor = new MyInterceptor(redisTemplate);
        //指定拦截的url
        String[] paths = {"/**"};
        registry.addInterceptor(myInterceptor).addPathPatterns(paths);
    }
}
