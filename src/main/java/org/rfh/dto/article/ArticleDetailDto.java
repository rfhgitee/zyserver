package org.rfh.dto.article;

import lombok.Data;

@Data
public class ArticleDetailDto {
    private Long id;
    private String title;
    private String content;
    private Integer authorId;
    private String authorName;
//    用户头像
    private String image;
    private String createdTime;
    private Integer views;
    private Integer likes;
}
