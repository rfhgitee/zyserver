package org.rfh.dto.article;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ArticleDto2 {
    private Long id;
    private String title;
    private Integer userId;
    private Integer categoryId;
    private String content;
    private String images;
    private String userName;
}
