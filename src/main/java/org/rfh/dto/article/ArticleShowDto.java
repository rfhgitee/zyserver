package org.rfh.dto.article;

import lombok.Data;

@Data
public class ArticleShowDto {
    private Long id;
    private String title;
    private String content;
    private Integer authorId;
    private String authorName;
    private String images;
    private Integer views;
    private Integer likes;
    private Integer categoryId;
    private String categoryName;
}

