package org.rfh.dto.article;

import lombok.Data;

import java.util.List;

@Data
public class ArticleCommentDto {
    private Integer id;
    private Integer userId;
    private String userName;
    //用户头像
    private String image;
    private String content;
    private String createdTime;
    private Integer likes;
    private List<ArticleCommentDto> children;
    //评论回复数
    private Integer childrenSize;
    //评论点赞
    private boolean isLiked = false;
}
