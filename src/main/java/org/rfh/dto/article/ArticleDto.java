package org.rfh.dto.article;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class ArticleDto {
    private Long id;
    private String title;
    private Integer userId;
    private Integer categoryId;
    private String content;
    private MultipartFile images;
    private String userName;
}
