package org.rfh.dto.article;

import lombok.Data;

@Data
public class ArticleChannelDto {
    private Integer categoryId;

    private Integer userId;

    private String categoryName;

    private String description;
}
