package org.rfh.dto.page;

import lombok.Data;

@Data
public class ArticlePageDto {
    private Integer pageNum;
    private Integer pageSize;
    private String categoryId;
    private Integer userId;
    //关键字
    private String keyWord;
}
