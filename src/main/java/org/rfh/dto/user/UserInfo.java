package org.rfh.dto.user;

import lombok.Data;

@Data
public class UserInfo {
    private Integer id;
    private String name;
    private String phone;
    private Boolean sex;
    private String image;
}
