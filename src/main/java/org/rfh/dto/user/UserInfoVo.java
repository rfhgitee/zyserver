package org.rfh.dto.user;

import lombok.Data;

@Data
public class UserInfoVo extends UserInfo{
    //是否被当前用户关注
    private Boolean isFollow;
}
