package org.rfh.dto.user;

import lombok.Data;

@Data
public class PasswordDto {
    Integer id;
    String oldPassword;
    String newPassword;
    String re_pwd;
}
