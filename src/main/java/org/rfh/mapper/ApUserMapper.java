package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.rfh.pojo.ApUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * APP用户信息表 Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-11-21
 */
@Mapper
public interface ApUserMapper extends BaseMapper<ApUser> {
    public List<ApUser> selectById();
}
