package org.rfh.mapper;

import org.rfh.pojo.ApUserFan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * APP用户粉丝信息表 Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2024-01-29
 */
public interface ApUserFanMapper extends BaseMapper<ApUserFan> {

}
