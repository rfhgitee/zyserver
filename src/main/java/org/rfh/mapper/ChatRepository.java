package org.rfh.mapper;

import org.rfh.pojo.Chat;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface ChatRepository extends MongoRepository<Chat, String> {
}
