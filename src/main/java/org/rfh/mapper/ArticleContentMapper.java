package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.rfh.pojo.ArticleContent;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 已发布文章内容表 Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-12-01
 */
@Mapper
public interface ArticleContentMapper extends BaseMapper<ArticleContent> {

}
