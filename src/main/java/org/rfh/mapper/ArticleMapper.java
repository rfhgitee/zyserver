package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.rfh.pojo.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 文章信息表，存储已发布的文章 Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-11-28
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {
    //查询历史浏览
    List<Article> selectArticleListPage(@Param("userId") Integer userId,  @Param("start") int start, @Param("pageSize") int pageSize);
    //查询点赞文章
    List<Article> selectLikeArticleListPage(@Param("userId") Integer userId,  @Param("start") int start, @Param("pageSize") int pageSize);
}
