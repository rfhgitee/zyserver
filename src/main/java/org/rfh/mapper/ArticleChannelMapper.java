package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.rfh.pojo.ArticleChannel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-11-23
 */
@Mapper
public interface ArticleChannelMapper extends BaseMapper<ArticleChannel> {

}
