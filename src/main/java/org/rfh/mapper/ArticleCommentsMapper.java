package org.rfh.mapper;

import org.rfh.pojo.ArticleComments;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
public interface ArticleCommentsMapper extends BaseMapper<ArticleComments> {

}
