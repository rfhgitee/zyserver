package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.rfh.pojo.ArticleViews;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-12-20
 */
@Mapper
public interface ArticleViewsMapper extends BaseMapper<ArticleViews> {
    //查看总条数
    Integer getTotal(@Param("userId") Integer userId);
}
