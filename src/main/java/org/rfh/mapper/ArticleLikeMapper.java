package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.rfh.pojo.ArticleLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@Mapper
public interface ArticleLikeMapper extends BaseMapper<ArticleLike> {
    //查总条数
    Integer getTotal(@Param("userId") Integer userId);
}
