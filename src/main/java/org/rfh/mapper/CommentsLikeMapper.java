package org.rfh.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.rfh.pojo.CommentsLike;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@Mapper
public interface CommentsLikeMapper extends BaseMapper<CommentsLike> {

}
