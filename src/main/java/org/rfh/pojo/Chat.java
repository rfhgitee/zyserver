package org.rfh.pojo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;

@Document(collection = "chat")
@Data
public class Chat implements Serializable {
    private String chatId;
    private Integer from;
    private Integer to;
    private LocalDateTime time;
    private String msg;
    private String fromAvatar;
    private String toAvatar;
}
