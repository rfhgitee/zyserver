package org.rfh.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 文章信息表，存储已发布的文章
 * </p>
 *
 * @author rfh
 * @since 2023-11-28
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="Article对象", description="文章信息表，存储已发布的文章")
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "标题")
    private String title;

    @ApiModelProperty(value = "文章作者的ID")
    private Integer authorId;

    @ApiModelProperty(value = "作者昵称")
    private String authorName;

    @ApiModelProperty(value = "文章所属频道ID")
    private Integer categoryId;

    @ApiModelProperty(value = "频道名称")
    private String categoryName;

    @ApiModelProperty(value = "文章布局	            0 无图文章	            1 单图文章	            2 多图文章")
    private Boolean layout;

    @ApiModelProperty(value = "文章标记	            0 普通文章	            1 热点文章	            2 置顶文章	            3 精品文章	            4 大V 文章")
    private Integer flag;

    @ApiModelProperty(value = "文章图片	            多张逗号分隔")
    private String images;

    @ApiModelProperty(value = "文章标签最多3个 逗号分隔")
    private String labels;

    @ApiModelProperty(value = "点赞数量")
    private Integer likes;

    @ApiModelProperty(value = "收藏数量")
    private Integer collection;

    @ApiModelProperty(value = "评论数量")
    private Integer comment;

    @ApiModelProperty(value = "阅读数量")
    private Integer views;

    @ApiModelProperty(value = "省市")
    private Integer provinceId;

    @ApiModelProperty(value = "市区")
    private Integer cityId;

    @ApiModelProperty(value = "区县")
    private Integer countyId;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;

    @ApiModelProperty(value = "发布时间")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime publishTime;

    @ApiModelProperty(value = "同步状态")
    private Boolean syncStatus;

    @ApiModelProperty(value = "来源")
    private Boolean origin;

    private String staticUrl;


}
