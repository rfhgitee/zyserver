package org.rfh.pojo;

import com.baomidou.mybatisplus.annotation.IdType;

import java.time.LocalDateTime;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ArticleComments对象", description="")
public class ArticleComments implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文章评论的ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "文章回复评论的ID")
    private Integer parentCommentId;

    @ApiModelProperty(value = "文章的ID")
    private Integer articleId;

    @ApiModelProperty(value = "用户的ID")
    private Integer userId;

    @ApiModelProperty(value = "评论内容")
    private String content;

    @ApiModelProperty(value = "评论点赞数")
    private Integer likes;

    @ApiModelProperty(value = "是否被删除")
    private Boolean isDeleted;

    @ApiModelProperty(value = "允许用户上传或附加图片、文件等附件的URL")
    private String attachmentUrl;

    @ApiModelProperty(value = "标识评论是否被标记为垃圾评论")
    private Boolean isSpam;

    private String reviewStatus;

    @ApiModelProperty(value = "创建时间")
    private LocalDateTime createdTime;


}
