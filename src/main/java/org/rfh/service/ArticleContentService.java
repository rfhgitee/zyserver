package org.rfh.service;

import org.rfh.pojo.ArticleContent;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 已发布文章内容表 服务类
 * </p>
 *
 * @author rfh
 * @since 2023-12-01
 */
public interface ArticleContentService extends IService<ArticleContent> {

}
