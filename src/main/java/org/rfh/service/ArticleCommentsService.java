package org.rfh.service;

import org.rfh.pojo.ArticleComments;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
public interface ArticleCommentsService extends IService<ArticleComments> {

}
