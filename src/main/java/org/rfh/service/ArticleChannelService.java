package org.rfh.service;

import org.rfh.pojo.ArticleChannel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rfh
 * @since 2023-11-23
 */
public interface ArticleChannelService extends IService<ArticleChannel> {

}
