package org.rfh.service;

import org.rfh.mapper.ChatRepository;
import org.rfh.pojo.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChatService {
    @Autowired
    private ChatRepository chatRepository;
    public void saveChat(Chat chat){
        chatRepository.save(chat);
    }
}
