package org.rfh.service;

import org.rfh.pojo.ApUserFan;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * APP用户粉丝信息表 服务类
 * </p>
 *
 * @author rfh
 * @since 2024-01-29
 */
public interface ApUserFanService extends IService<ApUserFan> {

}
