package org.rfh.service;

import org.rfh.pojo.ApUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * APP用户信息表 服务类
 * </p>
 *
 * @author rfh
 * @since 2023-11-21
 */
public interface ApUserService extends IService<ApUser> {

}
