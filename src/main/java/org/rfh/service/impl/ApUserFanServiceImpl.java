package org.rfh.service.impl;

import org.rfh.pojo.ApUserFan;
import org.rfh.mapper.ApUserFanMapper;
import org.rfh.service.ApUserFanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * APP用户粉丝信息表 服务实现类
 * </p>
 *
 * @author rfh
 * @since 2024-01-29
 */
@Service
public class ApUserFanServiceImpl extends ServiceImpl<ApUserFanMapper, ApUserFan> implements ApUserFanService {

}
