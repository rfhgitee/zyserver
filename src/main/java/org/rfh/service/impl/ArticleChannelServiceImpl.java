package org.rfh.service.impl;

import org.rfh.pojo.ArticleChannel;
import org.rfh.mapper.ArticleChannelMapper;
import org.rfh.service.ArticleChannelService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-11-23
 */
@Service
public class ArticleChannelServiceImpl extends ServiceImpl<ArticleChannelMapper, ArticleChannel> implements ArticleChannelService {

}
