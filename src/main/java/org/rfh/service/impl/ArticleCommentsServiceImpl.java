package org.rfh.service.impl;

import org.rfh.pojo.ArticleComments;
import org.rfh.mapper.ArticleCommentsMapper;
import org.rfh.service.ArticleCommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
@Service
public class ArticleCommentsServiceImpl extends ServiceImpl<ArticleCommentsMapper, ArticleComments> implements ArticleCommentsService {

}
