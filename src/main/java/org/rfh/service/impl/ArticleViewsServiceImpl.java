package org.rfh.service.impl;

import org.rfh.pojo.ArticleViews;
import org.rfh.mapper.ArticleViewsMapper;
import org.rfh.service.ArticleViewsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-20
 */
@Service
public class ArticleViewsServiceImpl extends ServiceImpl<ArticleViewsMapper, ArticleViews> implements ArticleViewsService {

}
