package org.rfh.service.impl;

import org.rfh.pojo.CommentsLike;
import org.rfh.mapper.CommentsLikeMapper;
import org.rfh.service.CommentsLikeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@Service
public class CommentsLikeServiceImpl extends ServiceImpl<CommentsLikeMapper, CommentsLike> implements CommentsLikeService {

}
