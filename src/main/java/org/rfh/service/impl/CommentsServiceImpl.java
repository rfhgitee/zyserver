package org.rfh.service.impl;

import org.rfh.pojo.Comments;
import org.rfh.mapper.CommentsMapper;
import org.rfh.service.CommentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-12
 */
@Service
public class CommentsServiceImpl extends ServiceImpl<CommentsMapper, Comments> implements CommentsService {

}
