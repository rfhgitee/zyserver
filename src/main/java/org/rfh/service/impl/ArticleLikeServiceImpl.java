package org.rfh.service.impl;

import org.rfh.pojo.ArticleLike;
import org.rfh.mapper.ArticleLikeMapper;
import org.rfh.service.ArticleLikeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
@Service
public class ArticleLikeServiceImpl extends ServiceImpl<ArticleLikeMapper, ArticleLike> implements ArticleLikeService {

}
