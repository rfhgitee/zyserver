package org.rfh.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.rfh.common.R;
import org.rfh.dto.article.ArticleDto2;
import org.rfh.dto.page.ArticlePageDto;
import org.rfh.mapper.ApUserMapper;
import org.rfh.mapper.ArticleContentMapper;
import org.rfh.pojo.ApUser;
import org.rfh.pojo.Article;
import org.rfh.mapper.ArticleMapper;
import org.rfh.pojo.ArticleChannel;
import org.rfh.pojo.ArticleContent;
import org.rfh.service.ApUserService;
import org.rfh.service.ArticleChannelService;
import org.rfh.service.ArticleContentService;
import org.rfh.service.ArticleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 文章信息表，存储已发布的文章 服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-11-28
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {
    @Autowired
    private ArticleMapper articleMapper;
    @Autowired
    private ArticleContentService articleContentService;
    @Autowired
    private ArticleChannelService articleChannelService;
    @Autowired
    private ApUserMapper apUserMapper;
    @Autowired
    private ApUserService apUserService;
    //分页操作
    public IPage<Article> pageList(ArticlePageDto articlePageDto) {
        Page<Article> articlePage = new Page<>();
        //设置当前页
        articlePage.setCurrent(articlePageDto.getPageNum());
        //设置每一页展示多少条数据
        articlePage.setSize(articlePageDto.getPageSize());
        //设置分类搜索条件
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        if(articlePageDto.getCategoryId() != null) {
            queryWrapper.eq(Article::getCategoryId,articlePageDto.getCategoryId());
        }
        queryWrapper.eq(Article::getAuthorId,articlePageDto.getUserId());
        articlePage = articleMapper.selectPage(articlePage, queryWrapper);
        return articlePage;
    }
//修改文章
    @Override
    @Transactional
    public R updateArticle(ArticleDto2 articleDto2) {
        //1.首先修改文章基本信息表
        String title = articleDto2.getTitle();
        String content = articleDto2.getContent();
        Integer categoryId = articleDto2.getCategoryId();
        String imageUrl = articleDto2.getImages();
        Long articleId = articleDto2.getId();
        //根据categoryId查询categoryName
        ArticleChannel articleChannel = articleChannelService.getById(categoryId);
        if(articleChannel == null) {
            return R.error("查询分类失败",500);
        }
        String categoryName = articleChannel.getCategoryName();
        LambdaUpdateWrapper<Article> articleLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        articleLambdaUpdateWrapper.eq(Article::getId,articleId)
                .set(Article::getTitle,title)
                .set(Article::getCategoryId,categoryId)
                .set(Article::getImages,imageUrl)
                .set(Article::getCategoryName,categoryName);
        boolean updateArticle = this.update(articleLambdaUpdateWrapper);
        if(!updateArticle){
            return R.error("修改文章基本信息失败",500);
        }
        //2.根据文章id修改文章内容表

        LambdaUpdateWrapper<ArticleContent> contentLambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        contentLambdaUpdateWrapper.eq(ArticleContent::getArticleId,articleId)
                .set(ArticleContent::getContent,content);
        boolean updateContent = articleContentService.update(contentLambdaUpdateWrapper);
        if(!updateContent){
            return R.error("修改文章内容信息失败",500);
        }
        return R.success(null);
    }
    //根据用户id返回用户
    @Transactional
    public ApUser getUserById(Integer id){
        LambdaQueryWrapper<ApUser> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(ApUser::getId,id);
        ApUser apUser = apUserService.getOne(lambdaQueryWrapper);
        return apUser;
    }

    @Override
    public IPage<Article> pageListAllArticle(ArticlePageDto articlePageDto) {
        Page<Article> articlePage = new Page<>();
        //设置当前页
        articlePage.setCurrent(articlePageDto.getPageNum());
        //设置每一页展示多少条数据
        articlePage.setSize(articlePageDto.getPageSize());
        //设置分类搜索条件
        LambdaQueryWrapper<Article> queryWrapper = new LambdaQueryWrapper<>();
        //设置关键字搜索
        if(!articlePageDto.getKeyWord().isEmpty()){
            queryWrapper.like(Article::getTitle,articlePageDto.getKeyWord());
        }
        //分类查询
        if(!articlePageDto.getCategoryId().isEmpty()){
            Integer id = new Integer(articlePageDto.getCategoryId());
            queryWrapper.eq(Article::getCategoryId,id);
        }
        articlePage = articleMapper.selectPage(articlePage, queryWrapper);
        return articlePage;
    }
}
