package org.rfh.service.impl;

import org.rfh.pojo.ApUser;
import org.rfh.mapper.ApUserMapper;
import org.rfh.service.ApUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * APP用户信息表 服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-11-21
 */
@Service
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {

}
