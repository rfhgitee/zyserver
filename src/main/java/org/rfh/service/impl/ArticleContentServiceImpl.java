package org.rfh.service.impl;

import org.rfh.pojo.ArticleContent;
import org.rfh.mapper.ArticleContentMapper;
import org.rfh.service.ArticleContentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 已发布文章内容表 服务实现类
 * </p>
 *
 * @author rfh
 * @since 2023-12-01
 */
@Service
public class ArticleContentServiceImpl extends ServiceImpl<ArticleContentMapper, ArticleContent> implements ArticleContentService {

}
