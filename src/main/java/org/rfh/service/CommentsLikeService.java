package org.rfh.service;

import org.rfh.pojo.CommentsLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
public interface CommentsLikeService extends IService<CommentsLike> {

}
