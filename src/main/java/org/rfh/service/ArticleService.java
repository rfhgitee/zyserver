package org.rfh.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.rfh.common.R;
import org.rfh.dto.article.ArticleDto2;
import org.rfh.dto.page.ArticlePageDto;
import org.rfh.pojo.ApUser;
import org.rfh.pojo.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 文章信息表，存储已发布的文章 服务类
 * </p>
 *
 * @author rfh
 * @since 2023-11-28
 */
public interface ArticleService extends IService<Article> {

   public  IPage<Article> pageList(ArticlePageDto articlePageDto);
   public R updateArticle(ArticleDto2 articleDto2);
   public ApUser getUserById(Integer id);
   public IPage<Article> pageListAllArticle(ArticlePageDto articlePageDto);
}
