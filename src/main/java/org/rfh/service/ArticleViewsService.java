package org.rfh.service;

import org.rfh.pojo.ArticleViews;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rfh
 * @since 2023-12-20
 */
public interface ArticleViewsService extends IService<ArticleViews> {

}
