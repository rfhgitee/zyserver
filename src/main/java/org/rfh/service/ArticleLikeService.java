package org.rfh.service;

import org.rfh.pojo.ArticleLike;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author rfh
 * @since 2023-12-19
 */
public interface ArticleLikeService extends IService<ArticleLike> {

}
