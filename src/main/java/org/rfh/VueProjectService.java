package org.rfh;

import jdk.jfr.internal.tool.Main;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("org.rfh.mapper")
@EnableTransactionManagement
public class VueProjectService {
    public static void main(String[] args) {
        SpringApplication.run(VueProjectService.class,args);
    }
}