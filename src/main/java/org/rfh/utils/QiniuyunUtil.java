package org.rfh.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import org.joda.time.DateTime;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;
@Slf4j
@Component
@ConfigurationProperties(prefix = "qiniuyun")
@PropertySource(value = "classpath:application.yml")
public class QiniuyunUtil {
    // 设置需要操作的账号的AK和SK
    @Value("${accessKey}")
    private String accessKey;
    @Value("${secretKey}")
    private String secretKey;
    // 要上传的空间
    @Value("${bucket}")
    private String bucket;
    //外链地址
    @Value("${domain}")
    private String domain;
    /**
     * 上传文件并且返回文件地址
     *
     * @param file 文件
     */
    public String setUploadManager(MultipartFile file) {
        // 密钥配置
        Auth auth = Auth.create(accessKey, secretKey);
        Configuration cfg = new Configuration(Zone.zone2());
        UploadManager uploadManager = new UploadManager(cfg);
        String qiniuyunImageDomain = domain;
        try {
            int dotPos = file.getOriginalFilename().lastIndexOf(".");
            if (dotPos < 0) {
                return null;
            }
            String fileExt = file.getOriginalFilename().substring(dotPos + 1).toLowerCase();
            // 判断是否是合法的文件后缀
//            if (!FileUtil.isFileAllowed(fileExt)) {return null;}
            String dataPath = new DateTime().toString("yyyy/MM/dd");
            String fileName =dataPath + "/"+ UUID.randomUUID().toString().replaceAll("-", "") + "." + fileExt;
            // 调用put方法上传
            Response res = uploadManager.put(file.getBytes(), fileName, auth.uploadToken(bucket));
            // 打印返回的信息
            if (res.isOK() && res.isJson()) {
                // 返回这张存储照片的地址
                return "http://"+ qiniuyunImageDomain + JSONObject.parseObject(res.bodyString()).get("key");
            } else {
                log.error("七牛异常:" + res.bodyString());
                return null;
            }
        } catch (IOException e) {
            // 请求失败时打印的异常的信息
            log.error("七牛异常:" + e.getMessage());
            return null;
        }

    }
}
