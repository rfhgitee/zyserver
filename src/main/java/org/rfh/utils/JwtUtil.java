package org.rfh.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtil {

    // 生成密钥
    private static final SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);

    // 设置过期时间为一小时
    private static final long EXPIRATION_TIME = 3600000;

    // 生成JWT Token
    public static String generateToken(String userId) {
        Date now = new Date();
        Date expiration = new Date(now.getTime() + EXPIRATION_TIME);

        Map<String, Object> claims = new HashMap<>();
        claims.put("userId", userId);

        return Jwts.builder()
                .setSubject(userId)
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiration)
                .signWith(SECRET_KEY, SignatureAlgorithm.HS512)
                .compact();
    }
    public static boolean validateToken(String token) {
        try {
            // 解析 token
            Claims claims =(Claims) parseToken(token);

            // 检查过期时间
            Date expiration = claims.getExpiration();
            if (expiration != null && expiration.after(new Date())) {
                // 验证签名
                String userId =(String) claims.get("userId");
                String signature = Jwts.builder()
                        .setSubject(userId)
                        .setClaims(claims)
                        .setIssuedAt(new Date())
                        .setExpiration(expiration)
                        .signWith(SECRET_KEY, SignatureAlgorithm.HS512)
                        .compact();

                return token.equals(signature);
            }
        } catch (Exception e) {
            // token 解析或验证失败
            e.printStackTrace();
        }
        return false;
    }
    // 解析JWT Token
    public static Map<String, Object> parseToken(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(SECRET_KEY)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public static void main(String[] args) {
        // 示例：根据用户 ID 生成并解析JWT Token
        String userId = "12345";
        String token = generateToken(userId);
        System.out.println("Generated Token: " + token);

        Claims parsedClaims =(Claims) parseToken(token);
        System.out.println("Parsed Claims: " + parsedClaims);
        System.out.println(parsedClaims.get("userId"));
        System.out.println(validateToken(token));
    }
}
